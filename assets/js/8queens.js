const canvas = document.getElementById('chessboard');
const ctx = canvas.getContext('2d');
const blackQueen = '/assets/img/black-queen.jpg';
const whiteQueen = '/assets/img/white-queen.jpg';
const size = 400;
canvas.width = size;
canvas.height = size;
const xQueenOffset = 15;
const yQueenOffset = 2.5;
const crossOffset = 15;
let queens = [];
let blocked = [];

const drawBorder = () => {
    ctx.lineWidth = 2;
    ctx.strokeRect(0,0,size,size);
}    

const isBlack = square => {
    const [x, y] = square;
    return (x + y) % 2 == 0;
}

const step = size / 8;

const drawSquares = () => {
    for (let x = 0; x < 8; x++) {
        for (let y = 0; y < 8; y++) {
            if (isBlack([x, y])) {
                ctx.fillRect(y * step, x * step, step, step);
            }
        }
    }
}

const squareFromCoordinates = coord => {
    return coord.map((x) => Math.ceil(x / step) - 1);
}

const coordinatesFromSquare = square => {
    return square.map((x) => x * step);
}

const drawQueen = square => {
    const img = new Image();
    const [x, y] = coordinatesFromSquare(square);
    img.onload = () => ctx.drawImage(img, x + xQueenOffset , y + yQueenOffset);
    img.src = isBlack(square) ? whiteQueen : blackQueen;
}

const drawCross = square => {
    const [x, y] = coordinatesFromSquare(square);
    ctx.strokeStyle = '#A52A2A';
    
    ctx.beginPath();
    ctx.moveTo(x + crossOffset, y + crossOffset);
    ctx.lineTo(x + step - crossOffset, y + step - crossOffset);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(x + step - crossOffset, y + crossOffset);
    ctx.lineTo(x + crossOffset, y + step - crossOffset);
    ctx.stroke();

    ctx.strokeStyle = '#000';
}

const getVertical = square => {
    // get the other squares in the vertical line of square
    const arr = [];
    const [x, y] = square;
    for (let i = 0; i < 8; i++) {
        if (x != i) arr.push([i, y]);
    }
    return arr;
}

const getHorizontal = square => {
    const arr = [];
    const [x, y] = square;
    for (let i = 0; i < 8; i++) {
        if (y != i) arr.push([x, i]);
    }
    return arr;
}

const getDiagonal = square => {
    const arr = [];
    const [x, y] = square;
    const diag = x - (7 - y);
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if (x != i && y != j) {
                const rank = i;
                const file = 7 - j;
                if (rank-file == diag) {
                    arr.push([i,j]);
                }
            }
        }
    }
    return arr;
}

const getAntiDiagonal = square => {
    const arr = [];
    const [x, y] = square;
    const diag = x + (7 - y);
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            if (x != i && y != j) {
                const rank = i;
                const file = 7 - j;
                if (rank+file == diag) {
                    arr.push([i,j]);
                }
            }
        }
    }
    return arr;
}

const getBlocked = square => {
    // add to blocked the squares that are newly blocked
    // by the placement of queen in square
    return [...getHorizontal(square), ...getVertical(square),
            ...getDiagonal(square), ...getAntiDiagonal(square)];
}

const areSquaresEqual = (x, y) => {
    return x[0] == y[0] && x[1] == y[1];
}

const addQueen = square => {
    queens.push(square);
    const blocks = getBlocked(square);
    blocked = [...blocked, ...blocks];
    draw();
}

const isBlocked = square => {
    // check if square is blocked by some queen placement
    return blocked.some(x => areSquaresEqual(x,square));
}

const isQueen = square => {
    return queens.some(x => areSquaresEqual(x,square));
}

const removeQueen = square => {
    queens = queens.filter(x => !areSquaresEqual(x,square));
    blocked = queens.map(x => getBlocked(x)).flat();

    ctx.clearRect(0,0, canvas.width, canvas.height);
    drawBoard();
    draw();
}

canvas.addEventListener("click", event => {
    let rect = canvas.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    const square = squareFromCoordinates([x,y]);

    if (isQueen(square)) {
        removeQueen(square);
    } else if (isBlocked(square)) {
        alert("This square is blocked!")
    } else {
        addQueen(square);
    }
})

document.getElementById("clear").addEventListener("click", event => {
    queens = [];
    blocked = [];

    ctx.clearRect(0,0, canvas.width, canvas.height);
    drawBoard();
    draw();
})

document.getElementById("solution").addEventListener("click", event => {
    queens = [[0,3], [1,5], [2,7], [3,1], [4,6], [5,0], [6,2], [7,4]];
    blocked = queens.map(x => getBlocked(x)).flat();

    ctx.clearRect(0,0, canvas.width, canvas.height);
    drawBoard();
    draw();
})

const drawBoard = () => {
    drawBorder();
    drawSquares();
}

const draw = () => {
    queens.map(x => drawQueen(x));
    blocked.map(x => drawCross(x));
}
window.requestAnimationFrame(draw);
window.requestAnimationFrame(drawBorder);
drawBoard();

