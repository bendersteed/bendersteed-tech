title: Tracking git commits in org-mode
date: 2021-08-30 12:50
tags: @tech, emacs, org
---
<p>
In the road to track more effectively my habits, and in the context of this article, my programming
habits I put together a simple system for tracking the <a href="https://git-scm.com/">git</a> commits I do on the various projects I'm
working on. As always <a href="https://www.gnu.org/software/emacs/">emacs</a>, <a href="https://magit.vc/">magit</a> and <a href="https://orgmode.org/">org-mode</a> make this pretty easy, and that's the reason that
I'm so heavily invested in emacs.
</p>

<p>
For the implementation of the above goal I use the <a href="https://github.com/magit/orgit">orgit library</a> that enhances the org-link system
with links for views in magit. This means that we can create hyperlinks in org-mode documents that
lead to specific magit buffers, hence creating links to descriptions of commits, branches, revision
etc.
</p>

<p>
To do so I configured the orgit package, using use-package:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">use-package</span> <span class="org-constant">orgit</span>
<span class="org-builtin">:ensure</span> t
<span class="org-builtin">:hook</span> <span class="org-rainbow-delimiters-depth-2">(</span>git-commit-post-finish . orgit-capture-after-commit<span class="org-rainbow-delimiters-depth-2">)</span>
<span class="org-builtin">:config</span>
<span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">defun</span> <span class="org-function-name">orgit-capture-after-commit</span> <span class="org-rainbow-delimiters-depth-3">()</span>
  <span class="org-doc">"Capture the commit into an org-mode file, decided in the approptriate capture template"</span>
  <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-keyword">let*</span> <span class="org-rainbow-delimiters-depth-4">(</span><span class="org-rainbow-delimiters-depth-5">(</span>repo <span class="org-rainbow-delimiters-depth-6">(</span>abbreviate-file-name default-directory<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span>
         <span class="org-rainbow-delimiters-depth-5">(</span>rev <span class="org-rainbow-delimiters-depth-6">(</span>magit-git-string <span class="org-string">"rev-parse"</span> <span class="org-string">"HEAD"</span><span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span>
         <span class="org-rainbow-delimiters-depth-5">(</span>link <span class="org-rainbow-delimiters-depth-6">(</span>format <span class="org-string">"orgit-rev:%s::%s"</span> repo rev<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span>
         <span class="org-rainbow-delimiters-depth-5">(</span>summary <span class="org-rainbow-delimiters-depth-6">(</span>substring-no-properties <span class="org-rainbow-delimiters-depth-7">(</span>magit-format-rev-summary rev<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span>
         <span class="org-rainbow-delimiters-depth-5">(</span>desc <span class="org-rainbow-delimiters-depth-6">(</span>format <span class="org-string">"%s (%s)"</span> summary repo<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
    <span class="org-rainbow-delimiters-depth-4">(</span>kill-new <span class="org-rainbow-delimiters-depth-5">(</span>format <span class="org-string">"[[%s][%s]]"</span> link desc<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
    <span class="org-rainbow-delimiters-depth-4">(</span>org-capture nil <span class="org-string">"g"</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>

<p>
The important thing here is that we added a hook on finishing a commit, that collects info for the
commit, pushes a orgit link for it in the kill-ring and then invokes org-capture. The relevant
template under the shortcut "g" is:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-string">"g"</span> <span class="org-string">"Git commit"</span> entry <span class="org-rainbow-delimiters-depth-2">(</span>file ,org-default-commit-file<span class="org-rainbow-delimiters-depth-2">)</span>
         <span class="org-string">"* %c \n:PROPERTIES:\n:CLOSED: %T\n:END:\n\n"</span>
         <span class="org-builtin">:immediate-finish</span> t<span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>

<p>
Briefly, this template takes the first element of the kill-ring and creates a new entry in the
org-default-commit-file with a CLOSED property and an active timestamp. This is important in order
to have them showing up in the agenda, which also requires to put the org-default-commit-file in the
agenda files.
</p>

<p>
Now every commit I make in any repo in my computer, using magit, will create an entry like this:
</p>


<div id="org8d3b742" class="figure">
<p><img src="../assets/img/2021-08-30_12-47-24_screenshot.png" alt="2021-08-30_12-47-24_screenshot.png" />
</p>
</div>

<p>
And at the same time I can review all the commits I've done in the agenda:
</p>


<div id="org030a9c5" class="figure">
<p><img src="../assets/img/2021-08-30_12-48-31_screenshot.png" alt="2021-08-30_12-48-31_screenshot.png" /></p>
</div>
