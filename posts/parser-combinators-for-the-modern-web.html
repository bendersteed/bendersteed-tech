title: Parser combinators for the "modern" web
date: 2020-07-22 02:08
tags: @tech, js, parsers, discodia, chan
---
<p>
The problem of parsing is a staple in computer science. Wikipedia gives the following definition:
</p>

<blockquote>
<p>
Parsing, syntax analysis, or syntactic analysis is the process of analyzing a string of symbols,
either in natural language, computer languages or data structures, conforming to the rules of a
formal grammar. The term parsing comes from Latin pars (orationis), meaning part (of speech).
</p>
</blockquote>

<p>
So in a way parsers are programs that impose or discover structure on generally unstructured text
data. This comes in handy for researching, formatting or even producing representations of data. In
general the input of parser is some textual data, and its output is an Abstract Syntax Tree (AST), a
hierarchical representation of the data structured along the rules.
</p>

<p>
In this post I will research a most elegant way to solve this problem: <a href="https://en.wikipedia.org/wiki/Parser_combinator">Parser combinators</a>. The people
of formal computer science education must be well acquitted with LL, LR or other traditional
parsers. However parser combinators are a novel way to quickly put together somewhat efficient
parsers. A parser combinator is a function that accepts several parsers and returns a new one. This
will become much more evident when we dive into the code, but the main idea is that we write simple
parsers for the basic elements of our "grammar" and then combine them to parse complex structures.
</p>

<p>
No method is of importance if it doesn't solve interesting problems. So our problem for this post is
to define a parser that will create an AST for image-board posts. To outline the problem better we
want to parse the following elements in a user submitted post:
</p>

<ol class="org-ol">
<li>Bold: words between "*".</li>
<li>Replies: series of digits that start with "&gt;&gt;".</li>
<li>Quotes: lines that start with "&gt;".</li>
<li>Links: in the markdown form of "[description](link)".</li>
<li>Simple text: everything that's not anything of the above.</li>
</ol>

<p>
Given that this is our grammar and also that we want to parse the input from a web form, we will use
js, in order to send nice structured elements to our back-end. Since the parser is quite efficient
to work on the front-end we can save the compiled html in our database. 
</p>

<p>
The parser outlined here is based on the research to solve the above problem for the
comments-imageboard for static blogs I'm developing under the name <a href="https://git.sr.ht/~bendersteed/discordia-chan-fe">discordia-chan</a>. So while it is a
mostly experimental code, I hope that at the same time will be good enough for small scale
production code.
</p>

<p>
To help with development we will use the <a href="https://github.com/francisrstokes/arcsecond">arcsecond</a> js library, that provides most of the utils we
need to define and combine plain parsers into the final one. The development of a minimal library is
also possible if we want to maximize performance or eliminate external dependencies. The structure
of our parser will be similar to the one used by <a href="https://github.com/jgm/pandoc">pandoc</a>, the "killer app" of the Haskell world and
one of the best software showcasing the power of parser combinators. For more info on the idea of
parser combinators you can check out <a href="https://wiki.haskell.org/Parsec">parsec</a>, the library behind pandoc.
</p>

<div id="outline-container-org741031b" class="outline-2">
<h2 id="org741031b">starting out simple</h2>
<div class="outline-text-2" id="text-org741031b">
<p>
To use arcsecond we first have to install it, canonically with:
</p>

<div class="org-src-container">
<pre class="src src-bash">npm install arcsecond
</pre>
</div>

<p>
Let's use ES6 imports to get some helper functions to start working with. These functions are mostly
parsers that take other parsers as inputs. If this sounds complicated, I hope it will become more
clear later on. 
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-keyword">import</span> <span class="org-rainbow-delimiters-depth-1">{</span> sequenceOf, many1, choice, letters, digits, optionalWhitespace,
         str, between, <span class="org-type">char</span>, many, regex, sepBy, anythingExcept <span class="org-rainbow-delimiters-depth-1">}</span> from <span class="org-string">"arcsecond"</span>;
</pre>
</div>

<p>
Remember that to run your program with nodejs, using ES6 imports, you have to use the esm module
like so (you may have to install it first):
</p>

<div class="org-src-container">
<pre class="src src-bash">node -r esm src/utils/parseTextarea.js
</pre>
</div>

<p>
So let's start working on our grammar requirements. Bold text is to be enclosed in * so we want to
parse text inside of *. This can be easily achieved by using the parser combinator between.
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-keyword">const</span> <span class="org-variable-name">betweenStars</span> = between <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-type">char</span><span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'*'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span> <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-type">char</span><span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'*'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;
<span class="org-keyword">const</span> <span class="org-variable-name">boldParser</span> = betweenStars <span class="org-rainbow-delimiters-depth-1">(</span>many<span class="org-rainbow-delimiters-depth-2">(</span>wordParser<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;

console.log<span class="org-rainbow-delimiters-depth-1">(</span>boldParser.run<span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">"*this is some bold text*"</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;
</pre>
</div>

<p>
Running this script in nodejs will give us the following result:
</p>
<div class="org-src-container">
<pre class="src src-js"><span class="org-rainbow-delimiters-depth-1">{</span>
  isError: <span class="org-constant">false</span>,
  result: <span class="org-rainbow-delimiters-depth-2">[</span> <span class="org-string">'this '</span>, <span class="org-string">'is '</span>, <span class="org-string">'some '</span>, <span class="org-string">'bold '</span>, <span class="org-string">'text'</span> <span class="org-rainbow-delimiters-depth-2">]</span>,
  index: 24,
  data: <span class="org-constant">null</span>
<span class="org-rainbow-delimiters-depth-1">}</span>
</pre>
</div>

<p>
So we have succesfully parsed the words that need to be bold into an array of results. The results
are in an array because each of them is parsed by wordParser and then boldParser checks for many of
them between "*". This way we combine the wordParser and the betweenStars parser into a more
powerful one. Still in the greater concept this is not yet that useful since we need to add some
information about what we parsed, in order to build our AST.
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-keyword">const</span> <span class="org-variable-name">boldParser</span> = betweenStars <span class="org-rainbow-delimiters-depth-1">(</span>many<span class="org-rainbow-delimiters-depth-2">(</span>wordParser<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>.map <span class="org-rainbow-delimiters-depth-1">(</span>results =&gt; <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-rainbow-delimiters-depth-3">{</span>
    type: <span class="org-string">'boldText'</span>,
    value: results.join <span class="org-rainbow-delimiters-depth-4">(</span><span class="org-string">''</span><span class="org-rainbow-delimiters-depth-4">)</span>
<span class="org-rainbow-delimiters-depth-3">}</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;
</pre>
</div>

<p>
In the second implementation of boldParser we are now giving a type to our data. This way we keep
the imprortant values, as well as the structure information needed for representing it later. Note
that map works on the results array by default. There are also ways to manipulate the data field for
stateful parsers, but these will be out of the context of this post.
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-rainbow-delimiters-depth-1">{</span>
  isError: <span class="org-constant">false</span>,
  result: <span class="org-rainbow-delimiters-depth-2">{</span> type: <span class="org-string">'boldText'</span>, value: <span class="org-string">'this is some bold text'</span> <span class="org-rainbow-delimiters-depth-2">}</span>,
  index: 24,
  data: <span class="org-constant">null</span>
<span class="org-rainbow-delimiters-depth-1">}</span>
</pre>
</div>

<p>
Those with keen eyes will have noticed that I have used the parser wordParser without introduction
or definition. I will introduce it later, but as of now think of it as a parser for everything
except special symbols in our text.
</p>

<p>
With these in mind and the use of of the various helpers from arcsecond we can define the rest cases
of our grammar. We can also define two helper functions join and output, that we can use to map over
our results.
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-keyword">const</span> <span class="org-variable-name">join</span> = array =&gt; array.join <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-string">''</span><span class="org-rainbow-delimiters-depth-1">)</span>;

<span class="org-comment-delimiter">// </span><span class="org-comment">notice the use of currying for easier application with map</span>
<span class="org-keyword">const</span> <span class="org-variable-name">output</span> = type =&gt; array =&gt; <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-rainbow-delimiters-depth-2">{</span>
  type: type,
  value: array
  <span class="org-rainbow-delimiters-depth-2">}</span><span class="org-rainbow-delimiters-depth-1">)</span>;

<span class="org-keyword">const</span> <span class="org-variable-name">replyParser</span> = sequenceOf<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-rainbow-delimiters-depth-2">[</span>
    str<span class="org-rainbow-delimiters-depth-3">(</span><span class="org-string">'&gt;&gt;'</span><span class="org-rainbow-delimiters-depth-3">)</span>,
    digits
<span class="org-rainbow-delimiters-depth-2">]</span><span class="org-rainbow-delimiters-depth-1">)</span>.map <span class="org-rainbow-delimiters-depth-1">(</span>results =&gt; output <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'reply'</span><span class="org-rainbow-delimiters-depth-2">)</span> <span class="org-rainbow-delimiters-depth-2">(</span>results<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;

<span class="org-keyword">const</span> <span class="org-variable-name">quoteParser</span> = sequenceOf<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-rainbow-delimiters-depth-2">[</span>
    <span class="org-type">char</span><span class="org-rainbow-delimiters-depth-3">(</span><span class="org-string">'&gt;'</span><span class="org-rainbow-delimiters-depth-3">)</span>,
    optionalWhitespace,
    wordParser
<span class="org-rainbow-delimiters-depth-2">]</span><span class="org-rainbow-delimiters-depth-1">)</span>.map <span class="org-rainbow-delimiters-depth-1">(</span>output <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'quote'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;

<span class="org-keyword">const</span> <span class="org-variable-name">betweenParens</span> = between <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-type">char</span><span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'('</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span> <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-type">char</span><span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">')'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;
<span class="org-keyword">const</span> <span class="org-variable-name">betweenBrackets</span> = between <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-type">char</span><span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'['</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span> <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-type">char</span><span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">']'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;
<span class="org-keyword">const</span> <span class="org-variable-name">urlParser</span> = sequenceOf<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-rainbow-delimiters-depth-2">[</span>
    choice<span class="org-rainbow-delimiters-depth-3">(</span><span class="org-rainbow-delimiters-depth-4">[</span>
        str<span class="org-rainbow-delimiters-depth-5">(</span><span class="org-string">'https://'</span><span class="org-rainbow-delimiters-depth-5">)</span>,
        str<span class="org-rainbow-delimiters-depth-5">(</span><span class="org-string">'http://'</span><span class="org-rainbow-delimiters-depth-5">)</span>,
        str<span class="org-rainbow-delimiters-depth-5">(</span><span class="org-string">'/'</span><span class="org-rainbow-delimiters-depth-5">)</span>
    <span class="org-rainbow-delimiters-depth-4">]</span><span class="org-rainbow-delimiters-depth-3">)</span>,
    letters,
    <span class="org-type">char</span><span class="org-rainbow-delimiters-depth-3">(</span><span class="org-string">'.'</span><span class="org-rainbow-delimiters-depth-3">)</span>,
    letters
<span class="org-rainbow-delimiters-depth-2">]</span><span class="org-rainbow-delimiters-depth-1">)</span>.map<span class="org-rainbow-delimiters-depth-1">(</span>join<span class="org-rainbow-delimiters-depth-1">)</span>;
<span class="org-keyword">const</span> <span class="org-variable-name">linkParser</span> = sequenceOf<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-rainbow-delimiters-depth-2">[</span>
    betweenBrackets<span class="org-rainbow-delimiters-depth-3">(</span>wordParser<span class="org-rainbow-delimiters-depth-3">)</span>,
    betweenParens<span class="org-rainbow-delimiters-depth-3">(</span>urlParser<span class="org-rainbow-delimiters-depth-3">)</span>
<span class="org-rainbow-delimiters-depth-2">]</span><span class="org-rainbow-delimiters-depth-1">)</span>.map <span class="org-rainbow-delimiters-depth-1">(</span>results =&gt; <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-rainbow-delimiters-depth-3">{</span>
    type: <span class="org-string">'link'</span>,
    url: results <span class="org-rainbow-delimiters-depth-4">[</span>1<span class="org-rainbow-delimiters-depth-4">]</span>,
    desc: results <span class="org-rainbow-delimiters-depth-4">[</span>0<span class="org-rainbow-delimiters-depth-4">]</span>
<span class="org-rainbow-delimiters-depth-3">}</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;

<span class="org-keyword">const</span> <span class="org-variable-name">plainParser</span> = wordParser.map <span class="org-rainbow-delimiters-depth-1">(</span>output <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'plain'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;
</pre>
</div>
</div>
</div>

<div id="outline-container-org6d6f17f" class="outline-2">
<h2 id="org6d6f17f">the elusive wordParser</h2>
<div class="outline-text-2" id="text-org6d6f17f">
<p>
The (probalby misnamed) wordParser that is the backbone of many of the afformentioned parsers is
assumed that it works likeso: it consumes anything, except the special symbols of our grammar. This
is an issue that I haven't completely solved, since I've not yet decided if I want to allow the out
of context use of these symbols. Naively the wordParser can be implemented as below:
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-keyword">const</span> <span class="org-variable-name">wordParser</span> = many1 <span class="org-rainbow-delimiters-depth-1">(</span>anythingExcept <span class="org-rainbow-delimiters-depth-2">(</span>regex <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-string">/^(\*|\]|\)|\n|&gt;|\(|\[)/</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>.map<span class="org-rainbow-delimiters-depth-1">(</span>join<span class="org-rainbow-delimiters-depth-1">)</span>;
</pre>
</div>

<p>
Two things are of notice here. Fist the very powerful regex parser. Second the problems that arise
by the implementation. If the symbols { *, [, ], &gt;, (, )} happen to appear in other uses apart from
their structural meaning, the rest of the text will be lost and not parsed.
</p>

<p>
This can be solved by implementing escapes, though I'm not sure this would be useful for inputing
comments on a web application.
</p>
</div>
</div>

<div id="outline-container-orga99c82a" class="outline-2">
<h2 id="orga99c82a">the power of combination</h2>
<div class="outline-text-2" id="text-orga99c82a">
<p>
With all our basic parsers in place we can now define what a paragraph is. For short texts,
like the ones we deal with hear, it's enough to assume that a paragraph is denoted by a newline. Our
work is mostly done, we just have to combine our work:
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-keyword">const</span> <span class="org-variable-name">paragraphParser</span> = many <span class="org-rainbow-delimiters-depth-1">(</span>choice <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-rainbow-delimiters-depth-3">[</span>
    replyParser,
    boldParser,
    urlParser,
    quoteParser,
    linkParser,
    plainParser
<span class="org-rainbow-delimiters-depth-3">]</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>.map <span class="org-rainbow-delimiters-depth-1">(</span>output <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'paragraph'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>;

<span class="org-keyword">export</span> <span class="org-keyword">const</span> <span class="org-variable-name">inputParser</span> = sepBy <span class="org-rainbow-delimiters-depth-1">(</span><span class="org-type">char</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-string">'\n'</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span> <span class="org-rainbow-delimiters-depth-1">(</span>paragraphParser<span class="org-rainbow-delimiters-depth-1">)</span>;
</pre>
</div>

<p>
So a paragraph is defined as many runs of a choice of parsers. We run the paragraphParser on every
body of text that is separated from any other with a newline and done! The result is a tree as the
following example:
</p>

<div class="org-src-container">
<pre class="src src-js"><span class="org-rainbow-delimiters-depth-1">[</span><span class="org-rainbow-delimiters-depth-2">{</span> type: <span class="org-string">'paragraph'</span>, value: <span class="org-rainbow-delimiters-depth-3">[</span>
    <span class="org-rainbow-delimiters-depth-4">{</span> type: <span class="org-string">'plain'</span>, value: <span class="org-string">"This is some text"</span> <span class="org-rainbow-delimiters-depth-4">}</span>,
    <span class="org-rainbow-delimiters-depth-4">{</span> type: <span class="org-string">'bold'</span>, value: <span class="org-string">"and some bold text."</span> <span class="org-rainbow-delimiters-depth-4">}</span>
<span class="org-rainbow-delimiters-depth-3">]</span><span class="org-rainbow-delimiters-depth-2">}</span>,
 <span class="org-rainbow-delimiters-depth-2">{</span> type: <span class="org-string">'paragraph'</span>, value: <span class="org-rainbow-delimiters-depth-3">[</span>
     <span class="org-rainbow-delimiters-depth-4">{</span> type: <span class="org-string">'quote'</span> value: <span class="org-string">"&gt; We quote someone."</span><span class="org-rainbow-delimiters-depth-4">}</span>
 <span class="org-rainbow-delimiters-depth-3">]</span><span class="org-rainbow-delimiters-depth-2">}</span>
<span class="org-rainbow-delimiters-depth-1">]</span>
</pre>
</div>

<p>
Our AST is ready for any transformation we want to impose on it!
</p>
</div>
</div>

<div id="outline-container-orgc5b9d8d" class="outline-2">
<h2 id="orgc5b9d8d">conclusion</h2>
<div class="outline-text-2" id="text-orgc5b9d8d">
<p>
The parser presented here is surely a naive implementation, and barely touches the surface of what
parser combinators can do. The case of recursive parsers as left as an exploration for the reader!
However with few lines of code and a quite transparent logic we can impose our structure to any
kinds of text. 
</p>

<p>
That's all for now! See you next time.
</p>
</div>
</div>
