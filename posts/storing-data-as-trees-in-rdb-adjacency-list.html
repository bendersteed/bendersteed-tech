title: Storing data as trees in RDB - Part 1: Adjacency List
date: 2020-03-26 06:53
tags: @tech, cl, db
---
<div id="outline-container-orga096d59" class="outline-2">
<h2 id="orga096d59">Models</h2>
<div class="outline-text-2" id="text-orga096d59">
<p>
The problem of solving hierarchical data in RDMSs is quite common
and old.  Data that adheres to some hierarchy is usually
represented in a tree.  This allows us to traverse the tree through
parent-child relations.  Elements in the same level of the tree are
then understood as elements that are incomparable (an antichain if
you want) or belonging to the same level of hierarchy.
</p>


<p>
The problem occured to me while developing <a href="https://wikisophy.bendersteed.tech">wikisophy</a>, a web app
that discovers and analyzes paths of the first links from
wikipedia articles.  It is conjectured that almost every wikipedia
articles leads to the "Philosophy" articles.  In order to gather
and store such data it is quite helpful to imagine them as a tree,
where parent is the article that is the href from the first link in
the child article. 
</p>


<p>
There are quite a few ways of storing such data in a relational
database.  In this article I will explore some of them, using
sqlite3 and Common Lisp.
</p>


<p>
The basic differences of the models are:
</p>
<ol class="org-ol">
<li>The way the relation is expressed.</li>
<li>The number of columns used in order to capture the relationships.</li>
<li>The cost of storing/retrieving parts of the tree for our needs.</li>
</ol>


<p>
So let's hop into some of them and see.
</p>
</div>
</div>


<div id="outline-container-org2f839ac" class="outline-2">
<h2 id="org2f839ac">Adjacency list model implementation</h2>
<div class="outline-text-2" id="text-org2f839ac">
<p>
So the plain simple approach is to just add a column that holds
the parent id of each data row.  I'm using the clsql system here
to speak to plain old sqlite database.
</p>

<p>
First we need to connect to our database. This concept doesn't
really mean anything for an sqlite database, but the clsql system
relies on the \*default-database\* global variable that is set
with the connect procedure.
</p>


<div class="org-src-container">
<pre class="src src-lisp"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">database-exists-p</span> <span class="org-rainbow-delimiters-depth-2">(</span>db<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Check if database DB exists. DB is an sqlite3 database's filename."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>clsql:probe-database db <span class="org-builtin">:database-type</span> <span class="org-builtin">:sqlite3</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">connect</span> <span class="org-rainbow-delimiters-depth-2">()</span>
  <span class="org-doc">"Initiate connection to *database*."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">when</span> <span class="org-rainbow-delimiters-depth-3">(</span>database-exists-p *database*<span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span>clsql:connect *database* <span class="org-builtin">:database-type</span> <span class="org-builtin">:sqlite3</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span>connect<span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>


<p>
Then we create our table:
</p>


<div class="org-src-container">
<pre class="src src-lisp"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defparameter</span> <span class="org-variable-name">*adjacency-list-model*</span>
  '<span class="org-rainbow-delimiters-depth-2">(</span><span class="org-rainbow-delimiters-depth-3">(</span>[id] integer <span class="org-builtin">:primary-key</span> <span class="org-builtin">:unique</span><span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span>[title] varchar <span class="org-builtin">:unique</span> <span class="org-builtin">:collate</span> nocase<span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span>[parent_id] integer<span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">create-adjacency-list-table</span> <span class="org-rainbow-delimiters-depth-2">(</span>table-name<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Create the table NAME in the DB."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">if</span> <span class="org-rainbow-delimiters-depth-3">(</span>not <span class="org-rainbow-delimiters-depth-4">(</span>clsql:table-exists-p table-name<span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span>
      <span class="org-rainbow-delimiters-depth-3">(</span>clsql:create-table table-name *adjacency-list-model*<span class="org-rainbow-delimiters-depth-3">)</span>
      <span class="org-string">"The table already exists!"</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span>create-adjacency-list-table <span class="org-string">"test_table"</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>


<p>
Now we have the model of our data in place.  The good thing about
the adjacency list model is that it requires very few
metadata.  The single "parent<sub>id</sub>" column is sufficient to express
the tree data structure.
</p>


<p>
Given that we have the table in place, the next step is to define
procedures for storing and retrieving the data we need.  For my
aforementioned web application, I developed a <a href="https://git.sr.ht/~bendersteed/wikisophy/tree/master/crawler.lisp">crawler</a> that returns
a list of the wikipedia articles that it encountered till reaching
the "Philosophy" article or an endpoint. Thus I need functions
that insert the list into the database, adding the correct
"parent<sub>id</sub>" for each element.
</p>


<div class="org-src-container">
<pre class="src src-lisp"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">get-id-by-title</span> <span class="org-rainbow-delimiters-depth-2">(</span>title<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Return the id of TITLE."</span> 
  <span class="org-rainbow-delimiters-depth-2">(</span>caar <span class="org-rainbow-delimiters-depth-3">(</span>clsql:select [id] <span class="org-builtin">:from</span> [adj_list] <span class="org-builtin">:where</span> [= [title] title] <span class="org-builtin">:collate</span> <span class="org-string">"nocase"</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">get-parent-by-title</span> <span class="org-rainbow-delimiters-depth-2">(</span>title<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Return the parent_id of TITLE."</span> 
  <span class="org-rainbow-delimiters-depth-2">(</span>caar <span class="org-rainbow-delimiters-depth-3">(</span>clsql:select [parent_id] <span class="org-builtin">:from</span> [adj_list] <span class="org-builtin">:where</span> [= [title] title]<span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">get-title-by-id</span> <span class="org-rainbow-delimiters-depth-2">(</span>id<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Return the title of ID."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>caar <span class="org-rainbow-delimiters-depth-3">(</span>clsql:select [title] <span class="org-builtin">:from</span> [adj_list] <span class="org-builtin">:where</span> [= [id] id]<span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">clear-underscores</span> <span class="org-rainbow-delimiters-depth-2">(</span>string<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Given a string replace any underscores with spaces."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>substitute #\SPACE #\_ string<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">insert-path</span> <span class="org-rainbow-delimiters-depth-2">(</span>path <span class="org-type">&amp;optional</span> parent-id<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Given a path of strings, insert them properly in database, by</span>
<span class="org-doc">  filling the appropriate parent_ids."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">when</span> path
    <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-keyword">let</span> <span class="org-rainbow-delimiters-depth-4">(</span><span class="org-rainbow-delimiters-depth-5">(</span>current <span class="org-rainbow-delimiters-depth-6">(</span>clear-underscores <span class="org-rainbow-delimiters-depth-7">(</span>car path<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
      <span class="org-rainbow-delimiters-depth-4">(</span><span class="org-keyword">when</span> <span class="org-rainbow-delimiters-depth-5">(</span>not <span class="org-rainbow-delimiters-depth-6">(</span>get-id-by-title current<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span>
          <span class="org-rainbow-delimiters-depth-5">(</span>clsql:insert-records <span class="org-builtin">:into</span> [adj_list]
                                <span class="org-builtin">:attributes</span> '<span class="org-rainbow-delimiters-depth-6">(</span>title parent_id<span class="org-rainbow-delimiters-depth-6">)</span>
                                <span class="org-builtin">:values</span> `<span class="org-rainbow-delimiters-depth-6">(</span>,current ,parent-id<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
      <span class="org-rainbow-delimiters-depth-4">(</span>insert-path <span class="org-rainbow-delimiters-depth-5">(</span>cdr path<span class="org-rainbow-delimiters-depth-5">)</span> <span class="org-rainbow-delimiters-depth-5">(</span>get-id-by-title current<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>


<p>
The recursive procedure "insert-path" is probably not the best way
to achieve this, but since my Lisp is way better than my ability
to write complex SQL queries I chose to sequence the inserts like
this.  The fact that the procedure is meant to be used with a
sqlite database also makes it less problematic since sqlite is
actually quite fast in chaining commands, given that it doesn't
have a server-client model.  Using a different RDB like postgresql
would be a different story though.
</p>


<p>
Also notice that the first recursion of the procedure runs always
without the parent-id argument, hence resulting in the first item
of the list getting inserted with a null value in the column. This
way it is declared as the root of our tree.
</p>


<p>
Lastly we have to to retrieve a path to "Philosophy".  By this I
mean that given an article title we want to get a list of articles
that are linked by the initial through the first article
principle. This is easily defined like-so:
</p>


<div class="org-src-container">
<pre class="src src-lisp"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">defun</span> <span class="org-function-name">get-path</span> <span class="org-rainbow-delimiters-depth-2">(</span>title<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-doc">"Given the title of an entry return it's path up to the root of the</span>
<span class="org-doc">  tree."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">let</span> <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-rainbow-delimiters-depth-4">(</span>parent <span class="org-rainbow-delimiters-depth-5">(</span>get-parent-by-title title<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-keyword">if</span> parent
        <span class="org-rainbow-delimiters-depth-4">(</span>cons <span class="org-rainbow-delimiters-depth-5">(</span>caar <span class="org-rainbow-delimiters-depth-6">(</span>clsql:select [title]
                                  <span class="org-builtin">:from</span> [adj_list]
                                  <span class="org-builtin">:where</span> [= [title] title]<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span>
         <span class="org-rainbow-delimiters-depth-5">(</span>get-path <span class="org-rainbow-delimiters-depth-6">(</span>get-title-by-id parent<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
        `<span class="org-rainbow-delimiters-depth-4">(</span>,<span class="org-rainbow-delimiters-depth-5">(</span>caar <span class="org-rainbow-delimiters-depth-6">(</span>clsql:select [title]
                               <span class="org-builtin">:from</span> [adj_list]
                               <span class="org-builtin">:where</span> [= [title] title]<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>
</div>
</div>


<div id="outline-container-org8717074" class="outline-2">
<h2 id="org8717074">Model assessment</h2>
<div class="outline-text-2" id="text-org8717074">
<p>
Having seen a plain implementation of the adjacency list model for
storing tree like data in a database, we can now discuss some
advantages and disadvantages of the model. 
</p>


<p>
For the good stuff we have:
</p>
<ol class="org-ol">
<li>It needs very few metadata columns.</li>
<li>Inserting an element is cheap, since it needs few metadata and
they are element only, so that there is no need for updates in
other elements.</li>
<li>Finding adjacent nodes is very cheap and the implementation can
be expanded to store children information as easily.</li>
</ol>


<p>
No all is great though.  The main problem with adjacency lists is
that retrieving subtrees is quite expensive.  To retrieve a trivial
subtree we have to make a sequential search along all its nodes.
This can't be optimized and can be hairy for long subtrees.
</p>


<p>
Doing some benchmarking proved that for my intended usage it may be
a good enough solution, since the paths I want to retrieve tend to
be quite short. I can't be sure yet though, so I will explore other
solutions as well.
</p>

<div class="org-src-container">
<pre class="src src-lisp">DATABASE&gt; <span class="org-rainbow-delimiters-depth-1">(</span>time <span class="org-rainbow-delimiters-depth-2">(</span>get-path <span class="org-string">"grendizer"</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>
Evaluation took:
  0.003 seconds of real time
  0.002415 seconds of total run time <span class="org-rainbow-delimiters-depth-1">(</span>0.002415 user, 0.000000 system<span class="org-rainbow-delimiters-depth-1">)</span>
  66.67% CPU
  6,023,658 processor cycles
  294,368 bytes consed

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-string">"Grendizer"</span> <span class="org-string">"Super Robot"</span> <span class="org-string">"Anime"</span> <span class="org-string">"Animation"</span> <span class="org-string">"Image"</span> <span class="org-string">"Depiction"</span>
 <span class="org-string">"Picture plane"</span> <span class="org-string">"Painting"</span> <span class="org-string">"Paint"</span> <span class="org-string">"Liquid"</span> <span class="org-string">"Compressibility"</span> <span class="org-string">"Thermodynamics"</span>
 <span class="org-string">"Physics"</span> <span class="org-string">"Natural science"</span> <span class="org-string">"Branch of science"</span> <span class="org-string">"Science"</span> <span class="org-string">"Testability"</span>
 <span class="org-string">"Empirical"</span> <span class="org-string">"Information"</span> <span class="org-string">"Uncertainty"</span> <span class="org-string">"Epistemic"</span> <span class="org-string">"Philosophy"</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>


<p>
Anyway, that's all for this part. In the next one I'll explore and
implement the nested set model and also present a hybrid model of
it and the adjacency list model. Till then take care of
yourselves. This pandemic is quite hard on everyone, so I hope you
find ways to pass this pause of everyday life creatively! Don't
hesitate to contact me for anything I may be able to help.
</p>
</div>
</div>
