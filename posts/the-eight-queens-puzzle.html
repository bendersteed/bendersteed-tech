title: The eight-queens puzzle
date: 2020-11-03 00:33
tags: @tech, sicp
---
<p>
The 8 queens puzzle is an almost two centuries problem that was proposed by Max Bezzel
in 1848. Although I had encountered it in the past in some combinatorics classes, I was recently
reminded of it while studying <a href="https://en.wikipedia.org/wiki/Structure_and_Interpretation_of_Computer_Programs">SICP</a>. In chapter 2.2.3 and in exercise 2.42 it is asked to implement a
procedure queens which solves the problem for any n*n chessboard board.
</p>

<p>
The main idea of the problem is placing 8 (or n) queens, in an 8*8 (or n*n) chessboard so that no
queen is in check from any other, meaning in the same row, column or diagonal. Visualizing the
problem a bit better, check the board and try to solve the problem here:
</p>

<canvas id="chessboard" style="display:block;margin:0 auto;"></canvas>
<div class="button-container" style="display:block;margin:0 auto;">
  <button class="button" id="clear">Clear</button>
  <button class="button" id="solution">Example solution</button>
</div>

<script src="/assets/js/8queens.js"></script>

<p>
There are many algorithmic solutions for this problem, some of which are explained in <a href="https://en.wikipedia.org/wiki/Eight_queens_puzzle">Wikipedia
entry</a> for the puzzle. In SICP it is proposed to find all solutions by means of recursion, so that we
check the nth queen placement given we have a n-1 column board where n-1 queens are placed
successfully.  
</p>

<p>
The skeleton given in the book and some helper functions follow:
</p>
<div class="org-src-container">
<pre class="src src-scheme"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">enumerate-interval</span> low high<span class="org-rainbow-delimiters-depth-2">)</span>
       <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">if</span> <span class="org-rainbow-delimiters-depth-3">(</span>&gt; low high<span class="org-rainbow-delimiters-depth-3">)</span>
           '<span class="org-rainbow-delimiters-depth-3">()</span>
           <span class="org-rainbow-delimiters-depth-3">(</span>cons low <span class="org-rainbow-delimiters-depth-4">(</span>enumerate-interval <span class="org-rainbow-delimiters-depth-5">(</span>+ low 1<span class="org-rainbow-delimiters-depth-5">)</span> high<span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">accumulate</span> op initial sequence<span class="org-rainbow-delimiters-depth-2">)</span>
       <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">if</span> <span class="org-rainbow-delimiters-depth-3">(</span>null? sequence<span class="org-rainbow-delimiters-depth-3">)</span>
           initial
           <span class="org-rainbow-delimiters-depth-3">(</span>op <span class="org-rainbow-delimiters-depth-4">(</span>car sequence<span class="org-rainbow-delimiters-depth-4">)</span>
               <span class="org-rainbow-delimiters-depth-4">(</span>accumulate op initial <span class="org-rainbow-delimiters-depth-5">(</span>cdr sequence<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">flatmap</span> proc seq<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>accumulate append '<span class="org-rainbow-delimiters-depth-3">()</span> <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-keyword">map</span> proc seq<span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">queens</span> board-size<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-function-name">queen-cols</span> k<span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-keyword">if</span> <span class="org-rainbow-delimiters-depth-4">(</span>= k 0<span class="org-rainbow-delimiters-depth-4">)</span>
        <span class="org-rainbow-delimiters-depth-4">(</span>list empty-board<span class="org-rainbow-delimiters-depth-4">)</span>
        <span class="org-rainbow-delimiters-depth-4">(</span>filter
         <span class="org-rainbow-delimiters-depth-5">(</span><span class="org-keyword">lambda</span> <span class="org-rainbow-delimiters-depth-6">(</span>positions<span class="org-rainbow-delimiters-depth-6">)</span> <span class="org-rainbow-delimiters-depth-6">(</span>safe? k positions<span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span>
         <span class="org-rainbow-delimiters-depth-5">(</span>flatmap
          <span class="org-rainbow-delimiters-depth-6">(</span><span class="org-keyword">lambda</span> <span class="org-rainbow-delimiters-depth-7">(</span>rest-of-queens<span class="org-rainbow-delimiters-depth-7">)</span>
            <span class="org-rainbow-delimiters-depth-7">(</span><span class="org-keyword">map</span> <span class="org-rainbow-delimiters-depth-8">(</span><span class="org-keyword">lambda</span> <span class="org-rainbow-delimiters-depth-9">(</span>new-row<span class="org-rainbow-delimiters-depth-9">)</span>
                   <span class="org-rainbow-delimiters-depth-9">(</span>adjoin-position new-row k rest-of-queens<span class="org-rainbow-delimiters-depth-9">)</span><span class="org-rainbow-delimiters-depth-8">)</span>
                 <span class="org-rainbow-delimiters-depth-8">(</span>enumerate-interval 1 board-size<span class="org-rainbow-delimiters-depth-8">)</span><span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span>
          <span class="org-rainbow-delimiters-depth-6">(</span>queen-cols <span class="org-rainbow-delimiters-depth-7">(</span>- k 1<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>queen-cols board-size<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>

<p>
So to start filling in the gaps we need to decide on the representation of our board and
positions. I choose to represent a position of two elements and a board as a list of such positions:
</p>
<div class="org-src-container">
<pre class="src src-scheme"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">pos</span> x y<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>list x y<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">pos-row</span> pos<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>car pos<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">pos-col</span> pos<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>cadr pos<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">pos-diagonal?</span> pos1 pos2<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>= <span class="org-rainbow-delimiters-depth-3">(</span>abs <span class="org-rainbow-delimiters-depth-4">(</span>- <span class="org-rainbow-delimiters-depth-5">(</span>pos-row pos1<span class="org-rainbow-delimiters-depth-5">)</span> <span class="org-rainbow-delimiters-depth-5">(</span>pos-row pos2<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span>
     <span class="org-rainbow-delimiters-depth-3">(</span>abs <span class="org-rainbow-delimiters-depth-4">(</span>- <span class="org-rainbow-delimiters-depth-5">(</span>pos-col pos1<span class="org-rainbow-delimiters-depth-5">)</span> <span class="org-rainbow-delimiters-depth-5">(</span>pos-col pos2<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-function-name">empty-board</span> '<span class="org-rainbow-delimiters-depth-2">()</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>

<p>
Then we have to define the adjoin-position procedure that adds a new position to a set of positions
(board) and of course a safe? procedure that checks if a set with board-positions is valid. Return
to the definition of queens and check that every time we need only check the safety of the newly
placed queens at the kth column. The rest are already checked.
</p>

<div class="org-src-container">
<pre class="src src-scheme"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">adjoin-position</span> row col set<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>cons <span class="org-rainbow-delimiters-depth-3">(</span>pos row col<span class="org-rainbow-delimiters-depth-3">)</span> set<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">safe?</span> set board-size<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-string">"The newest queen, the one we need to check for safety, is in the car of set."</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-function-name">attack?</span> pos1 pos2<span class="org-rainbow-delimiters-depth-3">)</span>
      <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-keyword">or</span> <span class="org-rainbow-delimiters-depth-4">(</span>= <span class="org-rainbow-delimiters-depth-5">(</span>pos-row pos1<span class="org-rainbow-delimiters-depth-5">)</span> <span class="org-rainbow-delimiters-depth-5">(</span>pos-row pos2<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
          <span class="org-rainbow-delimiters-depth-4">(</span>pos-diagonal? pos1 pos2<span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">let*</span> <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-rainbow-delimiters-depth-4">(</span>new-queen <span class="org-rainbow-delimiters-depth-5">(</span>car set<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
         <span class="org-rainbow-delimiters-depth-4">(</span>rest <span class="org-rainbow-delimiters-depth-5">(</span>cdr set<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span>accumulate <span class="org-rainbow-delimiters-depth-4">(</span><span class="org-keyword">lambda</span> <span class="org-rainbow-delimiters-depth-5">(</span>new-pos results<span class="org-rainbow-delimiters-depth-5">)</span>
                  <span class="org-rainbow-delimiters-depth-5">(</span><span class="org-keyword">and</span> <span class="org-rainbow-delimiters-depth-6">(</span>not <span class="org-rainbow-delimiters-depth-7">(</span>attack? new-queen new-pos<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span>
                       results<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span>
                #t
                rest<span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>

<p>
Or algorithm is checking each row for every column and eagerly rejects the solutions when a column
of no safe positions appear. In this way it can be quite performant and gives solutions of boards up
to size 12 in acceptable time. 
</p>

<p>
One way we could improve the performance is to change our data representation for the positions from
a list to just a number. This way a board can be a single list and by so we will reduce the amount
of cons cells created. 
</p>

<p>
To do so we need to think about how to check the diagonal attacks:
</p>
<div class="org-src-container">
<pre class="src src-scheme"><span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">adjoin-position</span> row col set<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span>cons row set<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-function-name">safe?</span> k positions<span class="org-rainbow-delimiters-depth-2">)</span>
  <span class="org-rainbow-delimiters-depth-2">(</span><span class="org-keyword">let</span> <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-rainbow-delimiters-depth-4">(</span>new-queen <span class="org-rainbow-delimiters-depth-5">(</span>car positions<span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span><span class="org-keyword">define</span> <span class="org-rainbow-delimiters-depth-4">(</span><span class="org-function-name">iter</span> rest diagonal anti-diagonal<span class="org-rainbow-delimiters-depth-4">)</span>
      <span class="org-rainbow-delimiters-depth-4">(</span><span class="org-keyword">cond</span> <span class="org-rainbow-delimiters-depth-5">(</span><span class="org-rainbow-delimiters-depth-6">(</span>null? rest<span class="org-rainbow-delimiters-depth-6">)</span> #t<span class="org-rainbow-delimiters-depth-5">)</span>
            <span class="org-rainbow-delimiters-depth-5">(</span><span class="org-rainbow-delimiters-depth-6">(</span>= new-queen <span class="org-rainbow-delimiters-depth-7">(</span>car rest<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span> #f<span class="org-rainbow-delimiters-depth-5">)</span>
            <span class="org-rainbow-delimiters-depth-5">(</span><span class="org-rainbow-delimiters-depth-6">(</span>= diagonal <span class="org-rainbow-delimiters-depth-7">(</span>car rest<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span> #f<span class="org-rainbow-delimiters-depth-5">)</span>
            <span class="org-rainbow-delimiters-depth-5">(</span><span class="org-rainbow-delimiters-depth-6">(</span>= anti-diagonal <span class="org-rainbow-delimiters-depth-7">(</span>car rest<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span> #f<span class="org-rainbow-delimiters-depth-5">)</span>
            <span class="org-rainbow-delimiters-depth-5">(</span>#t <span class="org-rainbow-delimiters-depth-6">(</span>iter <span class="org-rainbow-delimiters-depth-7">(</span>cdr rest<span class="org-rainbow-delimiters-depth-7">)</span>
                      <span class="org-rainbow-delimiters-depth-7">(</span>- diagonal 1<span class="org-rainbow-delimiters-depth-7">)</span>
                      <span class="org-rainbow-delimiters-depth-7">(</span>+ anti-diagonal 1<span class="org-rainbow-delimiters-depth-7">)</span><span class="org-rainbow-delimiters-depth-6">)</span><span class="org-rainbow-delimiters-depth-5">)</span><span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span>
    <span class="org-rainbow-delimiters-depth-3">(</span>iter <span class="org-rainbow-delimiters-depth-4">(</span>cdr positions<span class="org-rainbow-delimiters-depth-4">)</span> <span class="org-rainbow-delimiters-depth-4">(</span>- new-queen 1<span class="org-rainbow-delimiters-depth-4">)</span> <span class="org-rainbow-delimiters-depth-4">(</span>+ new-queen 1<span class="org-rainbow-delimiters-depth-4">)</span><span class="org-rainbow-delimiters-depth-3">)</span><span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>

<span class="org-rainbow-delimiters-depth-1">(</span>length <span class="org-rainbow-delimiters-depth-2">(</span>queens 12<span class="org-rainbow-delimiters-depth-2">)</span><span class="org-rainbow-delimiters-depth-1">)</span>
</pre>
</div>

<p>
With this representation we can reach up to (queens 13) and our program is faster and less memory
hungry. This optimization was possible by working with abstract procedures that allow us to change
the underlying data forms. To change the program we needed only to provide two new functions.
</p>

<p>
SICP is surely a great book, and I think it's really worth spending the time on studying it
deeply. If I could propose any book to software engineers it would be this. 
</p>

<p>
🦉
</p>
