;;; Copyright © 2020 Dimakakos Dimakis <bendersteed@teknik.io>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (haunt asset)
	     (haunt page)
	     (haunt html)
	     (haunt post)
	     (haunt site)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
	     (haunt reader skribe)
	     (haunt reader commonmark)
	     (jakob reader html-prime)
	     (bd theme)
	     (bd static-pages)
	     (bd tag-pages))

(setlocale LC_ALL "en_US.UTF-8")

(site #:title "bendersteed.gr"
      #:domain "bendersteed.gr"
      #:default-metadata
      '((author . "bendersteed")
        (email  . "dimos@stinpriza.org"))
      #:readers (list html-reader-prime)
      #:builders (list
		  (blog #:prefix "/posts"
			#:theme bendersteed-haunt-theme)
		  index-page
		  resume-page
                  webring-page
		  tags->page
		  (atom-feed #:blog-prefix "/posts")
		  (static-directory "assets" "assets")))
