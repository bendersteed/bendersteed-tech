;;; Copyright © 2020 Dimakakos Dimakis <bendersteed@teknik.io>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (bd webring)
  #:use-module (srfi srfi-9)
  #:use-module (web client)             ; for receiving feeds
  #:use-module (ice-9 receive)          ; multiple-value handling for responses
  #:use-module (ice-9 threads)
  #:use-module (sxml simple)            ; for parsing the feeds
  #:use-module (sxml xpath)
  #:use-module (srfi srfi-19)
  #:use-module (haunt utils)
  #:use-module (bd utils)
  :export (get-recent-webring-entries
           weblink-site-title
           weblink-site-url
           weblink-post-url
           weblink-post-title
           weblink-date))

(setlocale LC_ALL "en_US.UTF-8")        ; for correct locales in date parsing

(define-record-type <weblink>
  (make-weblink site-title site-url post-title post-url date)
  weblink?
  (site-title weblink-site-title)
  (site-url weblink-site-url)
  (post-title weblink-post-title)
  (post-url weblink-post-url)
  (date weblink-date))

(define entry-keys-alist
  '((rss . item)
    (atom . http://www.w3.org/2005/Atom:entry)))

(define date-keys-alist
  '((rss . pubDate)
    (atom . http://www.w3.org/2005/Atom:updated)))

(define title-keys-alist
  '((rss . title)
    (atom . http://www.w3.org/2005/Atom:title)))

(define link-keys-alist
  '((rss . link)
    (atom . http://www.w3.org/2005/Atom:link)))

(define (url->sxml url)
  "Grab an xml file from the url, and return its sxml representation."
  (xml->sxml (call-with-values (lambda () (http-get url))
               (lambda (response body) body))))

(define (rss? feed)
  "Check if feed contains an rss node, to discern if it's rss or atom feed."
  (if (nil? ((select-kids (node-typeof? 'rss)) feed)) #f #t))

(define (parse-opml xml)
  "Parse an opml file and return a list of feed URLs.")

(define (parse-date date-string type)
  (define (parse-rfc2822-date date-string)
    (string->date date-string "~a, ~d ~b ~Y ~H:~M:~S ~z"))

  (define (parse-iso8601-date date-string)
    (string->date date-string "~Y-~m-~dT~H:~M:~S~z"))

  (case type
    ((rss) (parse-rfc2822-date date-string))
    ((atom) (parse-iso8601-date date-string))
    (else (error "Wrong type."))))

(define (get-date feed)            ; handle cases where these are not available
    (let ((type (if (rss? feed)
                    'rss
                    'atom)))
      (parse-date (cadr (car ((node-closure
                               (node-typeof? (assoc-ref date-keys-alist type)))
                              feed)))
                  type)))

(define (date> d1 d2)
  "Compare two dates, down to days. Return #T if in ascending order, else #F."
  (or
   (> (date-year d1) (date-year d2))
   (and (= (date-year d1) (date-year d2))
        (> (date-month d1) (date-month d2)))
   (and (= (date-year d1) (date-year d2))
        (= (date-month d1) (date-month d2))
        (> (date-day d1) (date-day d2)))))

(define (get-recent-webring-entries urls n)
  "Given a list of feeds, return the N most recently updated ones."
  (define (feed> f1 f2)
    (let ((d1 (get-date f1))
          (d2 (get-date f2)))
      (date> d1 d2)))

  (define (get-title xml type)
    (list-last
     (car ((node-closure (node-typeof? (assoc-ref title-keys-alist type))) xml))))

  (define (get-link xml type)
    (let ((link-node (car ((node-closure (node-typeof? (assoc-ref link-keys-alist type))) xml))))
      (if (eq? type 'rss)
          (list-last link-node)
          (cadr (list-last (list-last link-node))))))

  (define (parse-entry feed)
    (let* ((type (if (rss? feed)
                     'rss
                     'atom))
           (entry (car ((node-closure (node-typeof? (assoc-ref entry-keys-alist type))) feed))))
      (make-weblink (get-title feed type)
                    (get-link feed type)
                    (get-title entry type)
                    (get-link entry type)
                    (get-date feed))))

  (let ((feeds (par-map url->sxml urls)))
    (take-up-to n (par-map parse-entry (sort feeds feed>)))))
